
/*ScoresIA è un array di stringhe, SafeSearches sono stringhe*/
export function Compare(scoresIA, SafeSearch_adult, SafeSearch_violence){
    const security = ["incident", "fire", "alarm,blood", "corpse", "drug", "accident", "wreck", "disaster", "smoke", "light"];
    const publicspace = ["lawn", "public space", "green", "grass", "plant", "sports", "tree","player","hall","concert","park"];
    const road = ["street","road","manhole", "road sign","car","truck","hole","highway"];
    const building = ["building", "home","house","bridge", "floor","ceiling","structure"];
    const school = ["school", "children","child", "homework","book","pencil","playground"];

    if(SafeSearch_adult == 'VERY_LIKELY' || SafeSearch_adult == 'LIKELY'){
        return 'NonAccettato';
    }
    if(SafeSearch_violence == 'VERY_LIKELY' || SafeSearch_violence == 'LIKELY'){
        return 'Sicurezza';
    }

    var scores_security=0;
    var scores_school=0;
    var scores_road=0;
    var scores_building=0;
    var scores_publicSpace=0;

    security.forEach(function(preconfig) {
        scoresIA.forEach(function(output) {
            if(output==preconfig){
                scores_security +=1
            }  
        });
    });
    road.forEach(function(preconfig) {
        scoresIA.forEach(function(output) {
            if(output==preconfig){
                scores_road +=1
            }  
        });
    });
    building.forEach(function(preconfig) {
        scoresIA.forEach(function(output) {
            if(output==preconfig){
                scores_building +=1
            }  
        });
    });
    school.forEach(function(preconfig) {
        scoresIA.forEach(function(output) {
            if(output==preconfig){
                scores_school +=1
            }  
        });
    });
    publicspace.forEach(function(preconfig) {
        scoresIA.forEach(function(output) {
            if(output==preconfig){
                scores_publicSpace +=1
            }  
        });
    });
    var Out = [scores_building,scores_publicSpace,scores_road,scores_school,scores_security];
    if(Math.max(Out)==scores_building){
        return 'Edifici';
    }
    if(Math.max(Out)==scores_publicSpace){
        return 'Spazio Pubblico';
    }
    if(Math.max(Out)==scores_road){
        return 'Manutenzione strade comunali';
    }
    if(Math.max(Out)==scores_school){
        return 'Scuole';
    }
    if(Math.max(Out)==scores_security){
        return 'Sicurezza';
    }
    
};
