import React from 'react';
import {
    Switch,
    Route,
} from 'react-router-dom';
import InputView from './InputView';
import Login from './components/Login';

const MainRouter = () => (
  <main>
    <Switch>
        <Route exact path='/' component={Login}/>
        <Route path='/InputView' component={InputView}/>
    </Switch>
  </main>
);

export default MainRouter;