import React, { Component } from 'react';
import { 
    Typography, 
    AppBar, 
    TextField, 
    Button, 
    Paper, 
    Grid, 
    MenuItem,
    InputAdornment,
} from '@material-ui/core';
//import { GoogleVisionLabeling } from './SnippetAPI_rest';
//import { GoogleVisionSafe } from './SafeSearch';
import * as Compare from './Compare';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import store from './store/index';
import { addData } from './actions/index';
import Grazie from './components/Grazie';
import { writeData } from './FirebaseWorker';

export default class InputView extends Component {

    constructor(props) {
        super(props)

        this.state = {
            commento: "",
            image: null,
            classificazione: "",
            grazie: false,
        }
        this.location;
    }

    componentWillMount() {
        this.unsubscribe = store.subscribe(() =>
            console.log(store.getState())
        );
    }

    componentWillUnMount() {
        this.unsubscribe();
    }

    _handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    _getLocation = () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                store.dispatch(addData({luogo: position.coords.longitude + ', ' + position.coords.latitude}));
            });
        } else {
            this.location = "Geolocation is not supported by this browser.";
        }
    };

    _handleClick = (e) => {
        e.preventDefault();

            const data = {
                commento: this.state.commento,
                classificazione: this.state.classificazione,
                image: this.state.image
            };

            store.dispatch(addData(data));

            console.log(store.getState())
            this.setState({
                grazie: true,
            });

            writeData()
        
    }

    _fileChangedHandler = event => {
        event.preventDefault();

        var reader = new FileReader();
        var file = event.target.files[0];
        var self = this;
    
        reader.onloadend = () => {
            self.img = reader.result;
            

            this.setState({
                image: file,
            });
        }
        reader.readAsDataURL(file)
    };

    render() {
        let renderedImage;

        if(this.state.image === null) {
            renderedImage = (
                <img style={styles.image} src={require('./assets/imagePlaceholder.png')} alt=""/>
            );
        } else {
            renderedImage = (
                <img style={styles.image} src={this.img} />
            );
        }

        const types = [
            "Area pubblica",
            "Scuole",
            "Sicurezza",
            "Manutenzione strade comunali",
            "Edifici",
         ];

        return (
            <React.Fragment>
                <AppBar position="static" style={styles.appBar}>
                    <Typography style={styles.appBarLabel}>Riporta un problema</Typography>
                </AppBar>
                <div>
                    {this.state.grazie === false ? 
                    (
                        <Paper style={styles.container}>
                            <form onSubmit={(e) => this._handleClick(e)}>
                                <React.Fragment>
                                    <input
                                        accept="image/*"
                                        id="contained-button-file"
                                        type="file"
                                        style={{display:'none'}}
                                        onChange={(e) => this._fileChangedHandler(e)}
                                        required
                                    />
                                    <label htmlFor="contained-button-file" style={styles.label}>
                                        <Button variant="contained" component="span" style={styles.imageBtn}>
                                            {renderedImage}
                                        </Button>
                                    </label>
                                </React.Fragment>
                                <div style={styles.section}>
                                    <TextField
                                        id="commento"
                                        multiline={true}
                                        value={this.state.commento}
                                        label="Descrivi il problema"
                                        required
                                        onChange={this._handleChange('commento')}
                                        style={styles.centerText}
                                    />
                                    <div style={styles.separator}></div>
                                    <TextField
                                        select
                                        required
                                        label="Seleziona classificazione"
                                        value={this.state.classificazione}
                                        onChange={this._handleChange('classificazione')}
                                        style={styles.centerText}
                                    >
                                        {types.map(option => (
                                            <MenuItem key={option} value={option}>
                                            {option}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    <div style={styles.separator}></div>
                                    <TextField
                                        type="submit"
                                        variant="contained"
                                    />
                                </div>
                            </form>
                        </Paper>
                    ) : (
                        <Grazie />
                    )}
                </div>
            </React.Fragment>
        );
    }
}

const styles = {
    appBarLabel: {
        color: '#fff',
        fontSize: 20,
    },
    appBar: {
        height: 50,
        padding: 10,
        display: 'flex',
        flexDirection: 'row',
        marginBottom: 50,
        width: '100%',
    },
    image: {
        width: '100%'
    },
    container: {
        width: 400,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginLeft: 'auto', 
        marginRight: 'auto',
    },
    separator: {
        marginBottom: 30
    },
    section: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        padding: 20
    }
}

/*
<div style={styles.pos}>{this.state.pos}</div>
                <div style={styles.container}>
                    <div>{this.state.task}</div>
                    <form>
                        {this._renderForm()}    
                    </form>
                </div> */