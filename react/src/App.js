import React, { Component } from 'react';
import './App.css';
import InputView from './InputView';
import store from './store/index';
import { addData } from './actions/index';
import { BrowserRouter } from 'react-router-dom'

import Home from './Home';
import Login from './components/Login';
import MainRouter from './MainRouter';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <MainRouter />
      </React.Fragment>
    );
  }
}

export default App;
