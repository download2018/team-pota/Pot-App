// Imports the Google Cloud client library
export function GoogleVisionLabeling(image){

  const vision = require('@google-cloud/vision');
  const key = "AIzaSyDbifApykdHx3v2dpjxCiDK7wI8ZgtooUo";
  var request = require('request')
      
    var file = require('fs').readFileSync(image).toString('base64')
  
    var body = {
      requests: {
        image: {
          content: file
        },
        features: [
          {
            type: 'LABEL_DETECTION',
            maxResults: 7
          }
        ]
      }
    }
    
    var url = 'https://vision.googleapis.com/v1/images:annotate\?key\='+key
    
    request({
      url: url,
      method: 'POST',
      body: JSON.stringify(body)
    }, (err, res, body) => {
      console.log(err)
      var scores =[];
      var iterate = JSON.parse(body).responses[0].labelAnnotations;
      console.log(JSON.parse(body).responses[0].labelAnnotations)
      iterate.forEach(function(values) {
        scores.push(values["description"]);
      });
      return scores;
    })
  }
