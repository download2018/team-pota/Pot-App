export function GoogleVisionSafe(image){
  //const vision = require('@google-cloud/vision');
  //const key = "AIzaSyDbifApykdHx3v2dpjxCiDK7wI8ZgtooUo";
  //var request = require('request')
    
    var file = require('fs').readFileSync(image).toString('base64')
  
    var body = {
      requests: {
        image: {
          content: file
        },
        features: [
          {
            type: 'SAFE_SEARCH_DETECTION',
            maxResults: 7
          }
        ]
      }
    }
    
    var url = 'https://vision.googleapis.com/v1/images:annotate\?key\='+key
    
    request({
      url: url,
      method: 'POST',
      body: JSON.stringify(body)
    }, (err, res, body) => {
      console.log(err)
      var adult_result = JSON.parse(body).responses[0].safeSearchAnnotation.adult
      var violence_result = JSON.parse(body).responses[0].safeSearchAnnotation.violence
      return [adult_result,violence_result];
    })
  }