import React, { Component } from 'react';
import { Paper, CircularProgress } from '@material-ui/core';

export default class Grazie extends Component {



    render() {
        return (
            <Paper style={styles.container}>
                <div style={{textAlign:'center'}}>Grazie per la sua segnalazione, verrà elaborata al più presto!</div>

            </Paper>
        );
    }   
}

const styles = {
    container: {
        width: 400,
        height: 300,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginLeft: 'auto', 
        marginRight: 'auto',
        padding: 10,
    }
}