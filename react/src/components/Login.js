import React, { Component } from 'react';
import { 
    Button,
    TextField,
    Paper,
} from '@material-ui/core';
import store from '../store/index';
import { addData } from '../actions/index';
import { Link } from 'react-router-dom';
import Navigation from './Navigation';

import { anonymSignIn } from '../FirebaseWorker';

import '../style/homepage.css';

import Recaptcha from 'react-recaptcha';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStroopwafel } from '@fortawesome/free-solid-svg-icons'

class Login extends Component {

    constructor(props) {
        super(props);
    }

    state = {
        anonimoForm: false,
        nome: "",
        cognome: "",
        email: ""
    }

    componentWillMount() {
        this.unsubscribe = store.subscribe(() => {
            console.log(store.getState())
        })
    }

    componentWillUnMount() {
        this.unsubscribe();
    }

    _onLogin = type => {
        if(type === 'google') {

        } else if (type === 'facebook') {

        } else {
            this.setState({ anonimoForm: true, });
        }
    }

    _handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    _handleClick = () => {
        const data = {
            nome: this.state.nome,
            cognome: this.state.cognome,
            email: this.state.email,
        };

        store.dispatch(addData(data));

        anonymSignIn(); 

        this.props.history.push('/InputView')
    }

    render() {
        return (
            <React.Fragment>
                <Navigation />
                <Paper style={styles.loginContainer}>
                    <ul style={{listStyle: 'none', margin: 0, padding: 0,}}>
                        <li>
                            <Button variant="contained" style={styles.loginBtn} onClick={() => this._onLogin('facebook')}>Facebook</Button>
                        </li>
                        <li>
                            <Button variant="contained" style={styles.loginBtn} onClick={() => this._onLogin('google')}>Google</Button>
                        </li>
                        <li>
                            <Button variant="contained" style={styles.loginBtn} onClick={() => this._onLogin()}>Anonimo</Button>
                        </li>
                    </ul>
                </Paper>
                {this.state.anonimoForm === true ? 
                    (
                        <Paper style={styles.container}>
                            <form style={styles.form} onSubmit={this._handleClick}>
                                <TextField
                                    id="nome"
                                    type="text"
                                    required
                                    value={this.state.nome}
                                    label="Inserisci il tuo nome"
                                    onChange={this._handleChange('nome')}
                                /> 
                                <div style={styles.separator}></div> 
                                <TextField
                                    id="cognome"
                                    type="text"
                                    required
                                    value={this.state.cognome}
                                    label="Inserisci il tuo cognome"
                                    onChange={this._handleChange('cognome')}
                                /> 
                                <div style={styles.separator}></div> 
                                <TextField
                                    id="email"
                                    type="email"
                                    required
                                    value={this.state.email}
                                    label="Inserisci la tua email"
                                    onChange={this._handleChange('email')}
                                />   
                                <div style={styles.separator}></div> 
                                <TextField 
                                    type="submit"
                                    variant="contained"
                                />
                            </form>
                        </Paper>
                    )
                : null}
            </React.Fragment>
        );
    }
}

const styles = {
    loginContainer: {
        display:'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignContents: 'center',
        justifyContent: 'center',
        width: 350,
        height: 200,
        margin:'auto',
        padding: 'auto',
        marginTop: 20,
    },
    loginBtn: {
        width: 300,
        marginBottom: 20,
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        width: 200,
        margin: 'auto',
        padding: 5
    },
    separator: {
        marginBottom: 20
    },
    container: {
        width: 300,
        height: 255,
        position: 'absolute',
        top: 100,
        left: '40%',
        zIndex: 99999,
    },
}

export default Login;
