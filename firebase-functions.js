const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.createAccount = functions.auth.user().onCreate((userRecord, context) => {
	console.log(userRecord.uid);
	admin.database().ref("/utenti/"+userRecord.uid+"/status").set("ok");
	if(userRecord.email!==null)
		admin.database().ref("/utenti/"+userRecord.uid+"/email").set(userRecord.email);
	return true;
});


exports.smistamento = functions.database.ref('/utenti/{uid}/segnalazione')
    .onCreate((snapshot, context) => {
    const config = functions.config();
    // functions.config() returns your environment variables.
    // In this case I have an array of my admin users' email addresses in accessControlLists.adminUsers
    // It looks like ['chris@chrisesplin.com', 'some-other-admin@chrisesplin.com']
    var classificazione = snapshot.child("/classificazione").val();
    const data = snapshot.child("/data").val();
    const tags = snapshot.child("/tag").val();
    const luogo = snapshot.child("/luogo").val();
    const commenti = snapshot.child("/commenti").val();

	var obj = {"1":"Manutenzione strade comunali","2": "Scuole","3": "Sicurezza","4":"Spazio pubblico"};
	var elabora=false;var vieta=false;
	var utenti_old=" ";var commenti_old=" ";

	Object.keys(obj).forEach(function (key){
		var ref = admin.database().ref("/segnalazioni/"+obj[key]);
		ref.once("value", function(userSnapshot) {
		    	userSnapshot.forEach(function(userSnap) {

		    		if(userSnap.val()!=="ok"&&distanzaData(userSnap.child("data").val(),data)) {
		        		console.log("data < 30 gg");
		        		var tag;
				        	tag=userSnap.child("tag").val().split(",");	//tag separati da virgole
			        		var sum=0;
			        		console.log(tag);
			        		if (distance(userSnap.child("luogo").val().split(","),luogo.split(","))<0.5) {
								
								tag.forEach(function(childEl) {
			        				if (tags.includes(childEl)) 
			        					sum++;
			        				console.log("tag contati : "+sum);
			        			});
			        			
			        		}
			        		if (sum>=3){
			        			elabora=true;
			        			var nodo= userSnap.child("/id").val(); //segnalazione esistente
			        			classificazione=userSnap.child("/classificazione").val(); 
			        			utenti_old=userSnap.child("/user").val();
			        			commenti_old=userSnap.child("/commenti").val();
								admin.database().ref("utenti/"+context.auth.uid+"/occupato").once('value').then(snap => {

				        			if(utenti_old.indexOf(context.auth.uid)===-1&&!snap.exists()){
										admin.database().ref("/segnalazioni/"+classificazione+"/"+nodo+"/user").set(utenti_old+";"+context.auth.uid);
										admin.database().ref("/segnalazioni/"+classificazione+"/"+nodo+"/commenti").set(commenti_old+";"+commenti);
										
										admin.database().ref("utenti/"+context.auth.uid+"/occupato").set("si");
										vieta=true;
									}

									remove();
									return;
								}).catch(function(error) {
	    									return console.log("failed: " + error.message)
	  						});
			        	}
		        		
		    		}


		        });  
		    if (elabora) {
		    	return;
		    }
		             
	    
	    });
	    if (elabora) {
		    	return;
		}
	});
	
if (elabora) {
	return Promise.resolve();

}
else{
	admin.database().ref(("/segnalazioni/"+classificazione+"/"+context.auth.uid+data)).once('value').then(snap => {
		admin.database().ref(("/utenti/"+context.auth.uid+"/occupato")).once('value').then(snap1 => {

	  		if (!snap.exists()&&utenti_old.indexOf(context.auth.uid)===-1&&!snap1.exists()) {
				admin.database().ref("/segnalazioni/"+classificazione+"/"+context.auth.uid+data).set(snapshot.val());
				admin.database().ref("/segnalazioni/"+classificazione+"/"+context.auth.uid+data+"/id").set(context.auth.uid+data);
				admin.database().ref("utenti/"+context.auth.uid+"/occupato").set("si");
				remove();
				return Promise.resolve();
	  		}
	  		return Promise.resolve();
	  	}).catch(function(error) {
	    		return console.log("failed: " + error.message)
	  	});
	return null;
	}).catch(function(error) {
	    		return console.log("failed: " + error.message)
	  	});
	return null;
}
	


	function remove(){
		snapshot.ref.remove()
	  		.then(function() {
	    		return console.log("Remove node after smistamento.")
	  	})
	  		.catch(function(error) {
	    		return console.log("Remove failed: " + error.message)
	  	});
	}


	function distanzaData(dataVariabile,data) {
	  var date1=new Date(dataVariabile);
	  var date2=new Date(data);
	  var differenza=(date2-date1)/1000/60/60/24;
	  if(date1-date2>30)
	  	return false;
	  else
	  	return true;
	}
	
	function distance(luogo1, luogo2) {
		lon1=luogo1[0];
		lat1=luogo1[1];
		lon2=luogo2[0];
		lat2=luogo2[1];
  		var R = 6371; // Radius of the earth in km
  		var dLat = toRad(lat2-lat1);  // Javascript functions in radians
  		var dLon = toRad(lon2-lon1); 
  		var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          	Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
          	Math.sin(dLon/2) * Math.sin(dLon/2); 
  		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  		var d = R * c; // Distance in km

	    console.log("distanza "+d );
 		return d;
	}

	function toRad(degrees) {
 	 return degrees * Math.PI / 180;
	}
});


const MAX_LOG_COUNT = 9;
exports.truncate = functions.database.ref('utenti/{uid}/segnalazione').onWrite((change) => {
  const parentRef = change.after.ref;
  return parentRef.once('value').then((snapshot) => {
    if (snapshot.numChildren() > MAX_LOG_COUNT) {
      let childCount = 0;
      const updates = {};
      snapshot.forEach((child) => {
        if (++childCount <= snapshot.numChildren() - MAX_LOG_COUNT) {
          updates[child.key] = null;
        }
      });
      // Update the parent. This effectively removes the extra children.
      return parentRef.update(updates);
    }
    return null;
  });
});

exports.disponibile = functions.database.ref('utenti/{uid}/occupato').onCreate((snapshot, context) => {
		snapshot.ref.remove()
	  	.then(function() {
	    	return console.log("Remove node after smistamento.")
	  	})
	  	.catch(function(error) {
	    	return console.log("Remove failed: " + error.message)
	  	});
		return null;
});
